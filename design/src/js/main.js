//Изменение активной ссылки в меню
$(function () {
    var location = window.location.href; //определяем url текущей страницы
    //разбиваем url на части, которые отделены друг от друга
    // с помощью символа слэша («/»),
    // выбираем самую последнюю из них и прибавляем ей в начало слэш
    var cur_url = '/' + location.split('/').pop();

    $('.navbar-nav li').each(function () {
        // получение адреса ссылки, содержащейся в пункте меню
        var link = $(this).find('a').attr('href');

        if (cur_url == link){
            $(this).addClass('active');
        }
    });
});


$('.slider').slick({
    dots: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 700,
});

// achievement
$('.achievements-slider').slick({
    dots: true,
    infinite: true,
    slidesToShow: 4,
    centerMode: true,
    slidesToScroll: 1,
    variableWidth: true,
    autoplay: true,
    autoplaySpeed: 700,
});

//
// var removeElement = function (elementId) {
//     var element = document.getElementById(elementId);
//     element.parentNode.removeChild(element);
// }
//
// var addElement = function (parentId, elementTag, elementId, html) {
//     var p = document.getElementById(parentId);
//     var newElement = document.createElement(elementTag);
//     newElement.setAttribute('id', elementId);
//     newElement.innerHTML = html;
//     p.appendChild(newElement);
// }
//
//
// $(document).ready(function(){
//     $("#but").click(function(){
//         $.ajax({
//             type:'GET',
//             url: '../index.html', //почитать
//             dataType: 'html',
//             data: {
//                 id: 123
//             },
//             error: function (e, message) {
//                 alert(message);
//             },
//             success: function (response) {
//                 $(textLine1).append(response);
//             }
//         });
//         return false;
//     })
//
// });

