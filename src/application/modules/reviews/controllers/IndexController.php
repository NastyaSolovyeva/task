<?php

class Reviews_IndexController extends ItRocks_Controller_Action{
       
    public function indexAction(){
        $page = $this->_request->getParam('page', 1);
        $reviewsTable = new Reviews_Model_DbTable_Reviews();

        $reviews = $reviewsTable->getAll();

        $paginator = Zend_Paginator::factory($reviews);
        $paginator->setItemCountPerPage(7);
        $paginator->setCurrentPageNumber($page);

        $this->view->assign('paginator', $paginator);
        $this->view->assign('page', $page);
    }

    public function sliderAction() {
        $reviewsTable = new Reviews_Model_DbTable_Reviews();

        $reviews = $reviewsTable->getAll();

        $this->view->assign('items', $reviews);
    }

    public function showAction() {
        $id = $this->_request->getParam('id');

        $reviewsTable = new Reviews_Model_DbTable_Reviews();
        $review = $reviewsTable->find($id)->current();
        if (!$review) {
            $this->_helper->redirector('home', 'index', 'static');
        }

        $this->view->assign('review', $review);
    }

    public function createReviewAction(){

    }
}
